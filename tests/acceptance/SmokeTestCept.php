<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('perform actions and see result');
$I->amOnPage('/');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->see('Laravel v8.80.0'); // ! Тут часть фразы с вашей главной
$I->amOnPage('/furniture');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->see('Работа с классом "Кресло"'); // ! Тут часть фразы с вашей страницы
