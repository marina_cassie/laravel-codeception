<?php

namespace App\Contracts;

interface HomeFurniture
{
    public function getDestination(); //функция возвращает назначение (Для дачи, Для дома, Для офиса, Для общественных мест)

    public function getRoom(); //функция возвращает комнату (Балкон, Бар, Веранда, Гостиная, Детская, Кабинет, Мансарда, Мастерская, Мастер-спальня, Студия, Терраса)
}
