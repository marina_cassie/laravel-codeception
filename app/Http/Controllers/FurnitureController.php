<?php

namespace App\Http\Controllers;

use Faker\Extension\ExtensionNotFound;
use http\Exception\BadMessageException;
use Illuminate\Http\Request;
use App\Contracts\IkeaFurniture;
use Exception;
use App\Models\Armchair;
use PHPUnit\Framework\Constraint\ExceptionMessage;
use Psy\Exception\ThrowUpException;

//use App\Models\Furniture;

class FurnitureController extends Controller
{
    public function save(Request $request)
    {
        $validation = $request->validate([
                'name' => 'required|min:2|max:50'
            ]);

        try{
            $temp = false;
            if ($request->input('isUnfold') == "on")
                $temp = true;

            $arm = new Armchair($request->input('name'), $request->input('count'), $request->input('fillerType'), $temp);
            print $arm;
            print "----------------------" . "<br />";
            print "Есть ли на складе? " . $arm->checkStockAvailability() . "<br />";
            print "----------------------" . "<br />";
            $arm->break();
            print "Кресло при переноске уронили " . "<br />";

            //$arm->save();
            //return redirect()->route('furniture');

        } catch(Exception $e) {
            echo $e->getMessage();
        }

    }
}
