<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\IkeaFurniture as IkeaFurniture;

class FurnitureServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $default = config('furniture.default');
        $furniture = config("furniture.{$default}.class");

        $this->app->bind(
            IkeaFurniture::class, // the furniture interface
            $furniture
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
