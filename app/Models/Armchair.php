<?php

namespace App\Models;


use App\Contracts\HomeFurniture;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Furniture;
use App\Contracts\IkeaFurniture;

class Armchair extends Furniture implements IkeaFurniture, HomeFurniture
{
    private string $fillerType; //тип наполнителя обивки
    private bool $isUnfold; //кресло раскладное или нет
    private string $destination;
    private string $room;

    public function __construct(string $name, int $count, string $fillerType, bool $isUnfold, string $destination = "", string $room = "")
    {
        parent::__construct($name, $count);
        $this->fillerType = $fillerType;
        $this->isUnfold = $isUnfold;
        $this->destination = $destination;
        $this->room = $room;
    }

    public function checkStockAvailability(): string
    {
        if ($this->getCount() > 0 && !$this->isBroken){
            return "в наличии";
        } else {
            return "нет на складе";
        }

    }

    public function break()
    {
        $this->isBroken = true;
        echo "Кресло сломалось" . "<br />";
    }

    public function __toString()
    {
        return "Кресло: название - ". $this->name . ", количество - " . $this->count .
            ", тип наполнителя - " . $this->fillerType . ", раскладывается -" . $this->isUnfold . "<br />";
    }

    public function getDestination()
    {
        return $this->destination;
    }

    public function getRoom()
    {
        return $this->room;
    }

}
