<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Contracts\IkeaFurniture;

class Table extends Furniture implements IkeaFurniture
{
    private string $tableDesign;
    public float $cost;
    private bool $available;

    public function __construct(string $name, int $count, float $cost, string $tableDesign, bool $available = true)
    {
        parent::__construct();
        $this->cost = $cost;
        $this->tableDesign = $tableDesign;
        $this->available = $available;
    }

    public function checkStockAvailability(): bool
    {
        if ($this->getCount() > 0 && $this->available){
            return true;
        } else {
            return false;
        }
    }

    protected function break()
    {
        $this->isBroken = true;
        echo "Стол сломался" . "<br />";
    }

    public function __toString()
    {
        echo "Информация о столе:". "br /";
        foreach ($this as $name => $value) {
            echo "$name: $value";
            echo "<br />";
        }
    }

}
