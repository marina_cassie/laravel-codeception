<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

abstract class Furniture extends Model
{
    public static int $id;
    public string $name;
    protected int $count;
    private ?string $country;
    protected bool $isBroken;

    public function __construct(string $name, int $count)
    {
        //self::$id++;
        $this->name = $name;
        $this->count = $count;
        $this->isBroken = false;
    }

    public function getID()
    {
        return Furniture::$id;
    }

    //геттеры и сеттеры
    protected function getCount(): int
    {
        return $this->count;
    }

    protected function setCount(int $count): void
    {
        $this->count = $count;
    }

    protected function getName(): string
    {
        return $this->name;
    }

    protected function setName(string $name): void
    {
        $this->name = $name;
    }

    abstract protected function break();
}
