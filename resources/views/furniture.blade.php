@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Работа с классом "Кресло"') }}</div>

                    <div class="card-body">
                        <form action="{{ route('armchair-form') }}" method="post">
                            @csrf
                            @if ($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li> {{ $error }} </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="name">Название</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{ old('name') }}" required >

                                <label for="count">Количество</label>
                                <input type="number" class="form-control" id="count" name="count" placeholder="Количество" value="{{ old('count') }}" required >

                                <label for="fillerType">Тип наполнителя обивки</label>
                                <input type="text" class="form-control" id="fillerType" name="fillerType" placeholder="Тип наполнителя обивки" value="{{ old('fillerType') }}" required >

                                <label for="isUnfold">Возможность разложить кресло</label>
                                <input type="checkbox" class="form-control" id="isUnfold" name="isUnfold"  >

                            </div>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
{{--                    <div class="card-header">{{ __('Работа с функциями класса "Кресло"') }}</div>--}}

{{--                    </div>--}}

                </div>
            </div>
        </div>
    </div>
@endsection
