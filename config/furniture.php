<?php

//use App\Models\Armchair;
//use App\Models\Table;

return [
    'default' => env('FURNITURE', 'armchair'),

    'armchair' => [
        'class' => App\Models\Armchair::class,
    ],

    'table' => [
        'class' => App\Models\Table::class,
    ]
];
